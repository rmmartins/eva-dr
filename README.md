# EVA-DR

Exploratory Visual Analysis using Dimensionality Reduction

# External dependencies

* https://github.com/rafaelmessias/Multicore-opt-SNE

# Datasets

Datasets can be read from a local `data` folder, as long as there is a custom `.py` file with a `load()` function that returns a dataframe and a list of labels in a tuple `df, labels`.
