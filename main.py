import plotly.io as pio
pio.templates.default = "none"

import pandas as pd
import dash
import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input, Output
import plotly.graph_objs as go
import dr
import datasets
import colorlover as cl
from ui import select_box


external_stylesheets = ['https://codepen.io/chriddyp/pen/bWLwgP.css']


app = dash.Dash(__name__, external_stylesheets=external_stylesheets)

sidebar_children = [
    select_box("Dataset", datasets.names(), "datasets"),
    select_box("Dim. Reduction", dr.names(), "dr_methods"),                    
]

app.layout = html.Div(
    id='main-layout-div',
    children = [            
            html.Div(
                children=sidebar_children,
                style = {
                    'float':'left',
                    'display':'inline-block',
                    'width': '25%'
                }
            ),
            html.Div(
                id='main-view-div'
            )
        ]
)

@app.callback(
    Output('main-view-div', 'children'),
    [
        Input('datasets', 'value'),
        Input('dr_methods', 'value')
    ]
)
def update_output_div(dataset_name, dr_method):
    data, labels = datasets.load(dataset_name, normalize=True)
    proj = dr.project(data, dr_method)

    # Turn labels into numbers in order to show them with colors
    label_index = list(set(labels))
    labels_num = [label_index.index(x) for x in labels]

    # A trace is a basically a set of related visual elements
    trace = go.Scatter(
        x = proj[:,0],
        y = proj[:,1],        
        mode = 'markers',
        marker=dict(
            size=10, 
            color=labels_num,
            colorscale=cl.scales['3']['qual']['Set1']
        )
    )

    # List of traces that will be shown on the main view
    data = [trace]
    
    # Other configurations of the view layout
    layout = go.Layout(
        title = "Main",
        yaxis = dict(
            scaleanchor = 'x',
            scaleratio = 1.0
        )
    )

    g = dcc.Graph(
        id='main-view',
        figure=go.Figure(data=data, layout=layout),
        style={
            'height':'100vh',
            'width':'50%',
            'display':'inline-block'
        }
    )
    
    return g

if __name__ == '__main__':
    app.run_server(debug=True)