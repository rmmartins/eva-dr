from joblib import Memory
from sklearn.decomposition import PCA
from sklearn import manifold
import sys
import numpy as np

try:
    from bhtsne import bhtsne
except:
    pass

try:
    from MulticoreTSNE import MulticoreTSNE
except:
    pass

try:
    from OptSNE import OptSNE
except:
    pass

cachedir = "./cachedir"
mem = Memory(cachedir)

def names():
    names = ["PCA", "Isomap", "LocallyLinearEmbedding", "MDS", "SpectralEmbedding"]

    if "bhtsne" in sys.modules.keys():
        names.insert(1, "TSNE")

    if "MulticoreTSNE" in sys.modules.keys():
        names.insert(1, "MulticoreTSNE")

    if "OptSNE" in sys.modules.keys():
        names.insert(1, "OptSNE")

    return names

@mem.cache
def project(df, dr_name):
    output = None
    if dr_name == "PCA":
        output = PCA(n_components=2).fit_transform(df)
    elif dr_name == "TSNE":
        output = bhtsne.run_bh_tsne(df.values, perplexity=np.sqrt(df.shape[0]))
    elif dr_name == "MulticoreTSNE":
        output = MulticoreTSNE(n_jobs=4).fit_transform(df.values)
    elif dr_name == "OptSNE":
        tsne = OptSNE(n_jobs=4)
        output = tsne.fit_transform(df.values)
    else:
        output = getattr(manifold, dr_name)(n_components=2).fit_transform(df)
    return output
