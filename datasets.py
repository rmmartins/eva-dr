from sklearn import datasets, preprocessing
import pandas as pd
import glob
from importlib import import_module
import re
from os import path

SKLEARN_DATASETS = ["iris", "boston", "diabetes", "digits", "wine", "breast_cancer"]

def names():
    names = []
    names.extend(SKLEARN_DATASETS)
    for file in glob.glob("data/*.py"):
        names.append(path.basename(file))
    return names

def load(dataset_name, normalize=False):
    if dataset_name in SKLEARN_DATASETS:
        dataset = getattr(datasets, f"load_{dataset_name}")()
        data = dataset.data
        if normalize:
            data = preprocessing.MinMaxScaler().fit_transform(data)
        columns = dataset.feature_names if hasattr(dataset, 'feature_names') else None    
        df = pd.DataFrame(data, columns=columns)
        target = dataset.target
        if hasattr(dataset, 'target_names'):
            target = [dataset.target_names[x] for x in target]
        return df, target
    elif dataset_name[-3:] == ".py":
        module_name = f"data.{dataset_name[:-3]}"
        print(f"Importing {module_name}...")
        module = import_module(module_name)
        return module.load()
    else:
        raise Exception("Dataset not found.")
