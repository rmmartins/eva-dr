import dash_html_components as html
import dash_core_components as dcc


def select_box(title, options, id):
    title = html.Div(title)
    sel = dcc.Dropdown(
        id=id,
        options=[{'label':x, 'value':x} for x in options],
        value=options[0]
    )
    return html.Div([title, sel])
    